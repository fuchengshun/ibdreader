package schema

type Column struct {
	TableDef TableDef
	Name     string

	/**
	 * Ordinal position.
	 */
	Ordinal int64

	/**
	 * Value before parsing type to length, precision or scale.
	 * Only meaningful for toString method.
	 */
	FullType string

	/**
	 * Type is treated as <code>DATE_TYPE</code> as in below query, also column type
	 * in this framework. It should be in upper case.
	 *
	 * <pre>
	 *   SELECT COLUMN_NAME, COLUMN_TYPE ,DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, IS_NULLABLE,
	 *   COLUMN_DEFAULT, COLUMN_COMMENT
	 * FROM
	 *  INFORMATION_SCHEMA.COLUMNS
	 * where table_schema ='db' AND table_name  = 'tb';
	 * </pre>
	 *
	 * @see com.alibaba.innodb.java.reader.column.ColumnType
	 */
	Type string

	Nullable bool

	/**
	 * Define in column definition explicitly like
	 * <pre>
	 *   c1 BIGINT(20) NOT NULL PRIMARY KEY,
	 * </pre>
	 * then this will be ture for the single column.
	 * <p>
	 * If primary key is specified in table definition like
	 * <pre>
	 *   PRIMARY KEY (c1)
	 * </pre>
	 * then this will be false.
	 */
	IsPrimaryKey bool

	/**
	 * This represents display size for integer type, max variable length for varchar
	 * type or fixed length for char type. This can only be set internally.
	 */
	length int64

	/**
	 * For DECIMAL(M, D), D is the number of digits to the right of the decimal point
	 * (the scale). It has a range of 0 to 30 and must be no larger than M.
	 */
	Scale int64

	/**
	 * For DECIMAL(M, D), M is the maximum number of digits (the precision). It has a range
	 * of 1 to 65. It has a range of 0 to 30 and must be no larger than M.
	 * <p>
	 * Since MySQL 5.6, there has fractional seconds support for TIME, DATETIME, and TIMESTAMP
	 * values, with up to microseconds (6 digits) precision:
	 * <p>
	 * To define a column that includes a fractional seconds part, use the syntax type_name(fsp),
	 * where type_name is TIME, DATETIME, or TIMESTAMP, and fsp is the fractional seconds
	 * precision. For example:
	 * <p>
	 * CREATE TABLE t1 (t TIME(3), dt DATETIME(6));
	 * The fsp value, if given, must be in the range 0 to 6. A value of 0 signifies that there
	 * is no fractional part. If omitted, the default precision is 0. (This differs from the
	 * standard SQL default of 6, for compatibility with previous MySQL versions.)
	 */
	Precision int64

	/**
	 * Character data types (CHAR, VARCHAR, the TEXT types, ENUM, SET, and any synonyms) can
	 * include CHARACTER SET to specify the character set for the column. CHARSET is a synonym
	 * for CHARACTER SET.
	 */
	Charset string

	GolangCharset string

	Collation string

	CollationCaseSensitive bool // default false

	/**
	 * //TODO make sure this is the right way to implement
	 * For example, if table charset set to utf8, then it will consume up to 3 bytes for one character.
	 * if it is utf8mb4, then it must be set to 4.
	 */
	MaxBytesPerChar int64 //default = 1;

	/**
	 * //TODO
	 * For charset like utf8mb4, CHAR will be treated as VARCHAR.
	 * Note this can only be set internally.
	 */
	IsVarLenChar bool

	/**
	 * Holding attributes for ENUM and SET type.
	 */
	Attributes map[string]interface{}
}
