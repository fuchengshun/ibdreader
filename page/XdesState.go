package page

type XdesState string

const (

	/*
	 * free extent
	 * The extent is completely empty and unused,
	 * and should be present on the filespace's FREE list
	 */
	FREE = 1

	/*
	 * free fragment extent
	 * Some pages of the extent are used individually,
	 * and the extent should be present on the filespace's FREE_FRAG list
	 */
	FREE_FRAG = 2

	/*
	 * full extent
	 * All pages of the extent are used individually,
	 * and the extent should be present on the filespace's FULL_FRAG list
	 */
	FULL_FRAG = 3

	/*
	 * extent that belongs to one segment
	 * The extent is wholly allocated to a file segment.
	 * Additional information about the state of this extent
	 * can be derived from the its presence on particular
	 * file segment lists (FULL, NOT_FULL, or FREE)
	 */
	FSEG = 4
)
