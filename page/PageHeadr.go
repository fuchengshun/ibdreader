package page

/*
	https://dev.mysql.com/doc/internals/en/innodb-page-header.html
	The Page Header has 14 parts, as follows: (56 byte):
	+-----------------------+-------+------------------------------------------------------------------------------------------------------------------+
	+	Name 				+ Size 	+  	Remarks														  												   +
	+-----------------------+-------+------------------------------------------------------------------------------------------------------------------+
	+	PAGE_N_DIR_SLOTS 	+  2    +	number of directory slots in the Page Directory part; initial value = 2							               +
	+	PAGE_HEAP_TOP   	+  2    +	record pointer to first record in heap                     													   +
	+	PAGE_N_HEAP   		+  2    +	number of heap records; initial value = 2                                                                      +
	+	PAGE_FREE     		+  2    + 	record pointer to first free record                                                                            +
	+	PAGE_GARBAGE    	+  2    +	"number of bytes in deleted records"				                                                           +
	+	PAGE_LAST_INSERT    +  2    +	record pointer to the last inserted record                                                                     +
	+	PAGE_DIRECTION	    +  2    +	either PAGE_LEFT, PAGE_RIGHT, or PAGE_NO_DIRECTION                                                             +
	+	PAGE_N_DIRECTION	+  2    +  	number of consecutive inserts in the same direction, for example, "last 5 were all to the left"                +
	+	PAGE_N_RECS			+  2    +	number of user records											                                               +
	+	PAGE_MAX_TRX_ID		+  8	+	the highest ID of a transaction which might have changed a record on the page (only set for secondary indexes) +                                 +
	+	PAGE_LEVEL   		+  2    +	level within the index (0 for a leaf page)                                                                     +
	+	PAGE_INDEX_ID		+  8	+ 	identifier of the index the page belongs to                                                                    +
	+	PAGE_BTR_SEG_LEAF   +  10   +	"file segment header for the leaf pages in a B-tree" (this is irrelevant here)                                 +
	+	PAGE_BTR_SEG_TOP	+  10	+	"file segment header for the non-leaf pages in a B-tree" (this is irrelevant here)                             +
	+-----------------------+-------+------------------------------------------------------------------------------------------------------------------+
*/
