package page

type RecordType string

/** leaf page conventional record type */
const CONVENTIONAL = 0

/** non-leaf page node pointer record type */
const NODE_POINTER = 1

/** infimum record type */
const INFIMUM = 2

/** supremum record type */
const SUPREMUM = 3
