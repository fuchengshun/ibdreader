package page

import "fmt"

type Index struct {
	IndexHeader IndexHeader
	FsegHeader  FsegHeader
	Infimum     GenericRecord
	Supremum    GenericRecord
	DirSlots    []int64
}

func IndexReadSlice() Index {
	var idx Index
	// 36 bytes index header
	idx.IndexHeader = IndexHeaderReadSlice()
	if idx.IndexHeader.Format != "COMPACT" {
		fmt.Println("Index header is unreadable, only new-style page format is supported!!!")
	}
	return idx
}
