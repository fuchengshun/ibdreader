package page

import (
	"container/list"
)

const MAX_SEGMENT_SIZE = 256

type FspHdrXes struct {
	/**
	 * One FSP_HDR, a.k.a XDES, has maximum of 256 extents (or 16,384 pages, 256 MB)，
	 * Every extent is managed by a XDES Entry.
	 */
	Fsp      FspHeader
	XdesList list.List
}

func FspHdrXesReadSlice() FspHdrXes {
	var fhx FspHdrXes
	fhx.Fsp = FspHeaderReadSlice()
	for i := 0; i < MAX_SEGMENT_SIZE; i++ {
		xdes := XdesReadSlice()
		if xdes.state == "" {
			break
		}
		fhx.XdesList.PushBack(xdes)
	}
	return fhx
}
