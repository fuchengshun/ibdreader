package page

import "ibdreader/schema"

type GenericRecord struct {
	PageNumber int64
	Header     RecordHeader
	/**
	 * 记录在物理文件格式中转为
	 * 这个值表示primary key存储位置，不包含header，因为每个record header中的next record offset
	 * 是一个相对位置，等于上一个record的primary key的位置加上这个offset 就是下一个record的primary
	 * key位置。对于infimum和supremum来说就是literal字符串的起始位置。
	 */
	PrimaryKeyPosition int64

	TableDef schema.TableDef

	/**
	 * Record fields.
	 */
	Values []interface{}

	/**
	 * If the record is in non-leaf page, then this represents the child page number.
	 */
	ChildPageNumber int64
}

func GenericRecordReadSlice() GenericRecord {
	var gr GenericRecord
	return gr
}
