package page

import (
	"fmt"
	"ibdreader/slice"
)

const FRAG_ARRAY_SIZE = 32

type InodeEntry struct {
	/**
	 * 该Inode归属的Segment ID，若值为0表示该slot未被使用.
	 */
	SegmentId int64

	/**
	 * FSEG_NOT_FULL链表上被使用的Page数量.
	 */
	NumberOfPagesUsedInNotFullList int64

	/**
	 * 完全没有被使用并分配给该Segment的Extent链表.
	 */
	Free ListBaseNode

	/**
	 * 至少有一个page分配给当前Segment的Extent链表，全部用完时，转移到FSEG_FULL上，
	 * 全部释放时，则归还给当前表空间FSP_FREE链表.
	 */
	NotFull ListBaseNode

	/**
	 * 分配给当前segment且Page完全使用完的Extent链表.
	 */
	Full ListBaseNode

	/**
	 * The value 97937874 is stored as a marker that this file segment INODE entry
	 * has been properly initialized.
	 */
	MagicNumber int64

	/**
	 * 属于该Segment的独立Page。总是先从全局分配独立的Page，
	 * 当填满32个数组项时，就在每次分配时都分配一个完整的Extent，并在XDES PAGE中将其
	 * Segment ID设置为当前值. 总共存储32个记录项.
	 */
	FragArrayEntries [FRAG_ARRAY_SIZE]int64
}

func InodeEntryReadSlice() InodeEntry {
	var ie InodeEntry
	ie.SegmentId = slice.ReadLongInt64()
	ie.NumberOfPagesUsedInNotFullList = slice.ReadInt()
	ie.Free = ListBaseNodefromSlice()
	ie.NotFull = ListBaseNodefromSlice()
	ie.Full = ListBaseNodefromSlice()
	ie.MagicNumber = slice.ReadInt()
	if ie.SegmentId > 0 {
		if !(ie.MagicNumber == 97937874) {
			fmt.Println("MagicNumber is not right!")
		}
	}
	for i := 0; i < FRAG_ARRAY_SIZE; i++ {
		ie.FragArrayEntries[i] = slice.ReadUnsignedInt()
	}
	return ie
}
