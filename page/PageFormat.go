package page

type PageFormat string

const (
	REDUNDANT = 0
	COMPACT   = 1
	// DYNAMIC    = 2
	// COMPRESSED = 3
)
