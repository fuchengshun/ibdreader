package page

import "ibdreader/slice"

type FsegHeader struct {
	LeafPagesInodeSpace         int64
	LeafPagesInodePageNumber    int64
	LeafPagesInodeOffset        int64
	NonLeafPagesInodeSpace      int64
	NonLeafPagesInodePageNumber int64
	NonLeafPagesInodeOffset     int64
}

func FsegHeaderReadSlice() FsegHeader {
	var fh FsegHeader
	fh.LeafPagesInodeSpace = slice.ReadUnsignedInt()
	fh.LeafPagesInodePageNumber = slice.ReadUnsignedInt()
	fh.LeafPagesInodeOffset = slice.ReadUnsignedShort()
	fh.NonLeafPagesInodeSpace = slice.ReadUnsignedInt()
	fh.NonLeafPagesInodePageNumber = slice.ReadUnsignedInt()
	fh.NonLeafPagesInodeOffset = slice.ReadUnsignedShort()
	return fh
}
