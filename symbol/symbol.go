package symbol

const EMPTY = ""
const SPACE = " "
const SPACE_CHAR rune = ' '
const COMMA = ","
const LEFT_PARENTHESES = "("
const RIGHT_PARENTHESES = ")"
const BACKTICK = "`"
const SINGLE_QUOTE = "'"
const DOUBLE_QUOTE = "\""
const DOT = "."
