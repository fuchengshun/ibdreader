package constants

import "container/list"

/**
 * Constants.
 *
 */

const MAX_VAL = "max_val"

const MIN_VAL = "min_val"

const DEFAULT_GOLANG_CHARSET = "UTF-8"

const DEFAULT_MYSQL_CHARSET = "utf8"

const DEFAULT_MYSQL_COLLATION = "utf8_general_ci"

const COLUMN_ROW_ID = "_row_id_"

/**
 * for mysql 5.6 and 5.7 the root page is usually page 3, but in mysql8 there introduces SDI page,
 * which may make root page number down from 4.
 * But the toolkit can be compatible with such case, if root page is SDI page then it will continue
 * search the file until the first index page found.
 */
const ROOT_PAGE_NUMBER = 3

const PRECISION_LIMIT = 5

const DEFAULT_DATA_FILE_SUFFIX = ".ibd"

const INT_10000 = 10000
const INT_1000 = 1000
const INT_100 = 100
const INT_10 = 10

const MAX_PRECISION = 6

const COLUMN_ATTRIBUTE_ENUM_KEY = "_ENUM_"

const MAX_ONE_BYTE_ENUM_COUNT = 255

const PRIMARY_KEY_NAME = "PRIMARY_KEY"

const ZERO_DATETIME = "0000-00-00 00:00:00"

//	BYTES_OF_INFIMUM := []byte("infimum\0")
//	BYTES_OF_SUPREMUM := []byte("supremum")

type Constants struct {
	BYTES_OF_INFIMUM  []byte
	BYTES_OF_SUPREMUM []byte
	MAX_RECORD_1      list.List
	MAX_RECORD_2      list.List
	MAX_RECORD_3      list.List
	MAX_RECORD_4      list.List
	MAX_RECORD_5      list.List
	MIN_RECORD_1      list.List
	MIN_RECORD_2      list.List
	MIN_RECORD_3      list.List
	MIN_RECORD_4      list.List
	MIN_RECORD_5      list.List
	CONST_UNSIGNED    list.List
}

func setDefaultConstants() Constants {
	lmax1 := *list.New()
	lmax2 := *list.New()
	lmax3 := *list.New()
	lmax4 := *list.New()
	lmax5 := *list.New()

	lmin1 := *list.New()
	lmin2 := *list.New()
	lmin3 := *list.New()
	lmin4 := *list.New()
	lmin5 := *list.New()

	unsigned := *list.New()
	unsigned.PushBack("UNSIGNED")
	unsigned.PushBack("unsigned")

	lmax1.PushBack(MAX_VAL)

	lmax2.PushBack(MAX_VAL)
	lmax2.PushBack(MAX_VAL)

	lmax3.PushBack(MAX_VAL)
	lmax3.PushBack(MAX_VAL)
	lmax3.PushBack(MAX_VAL)

	lmax4.PushBack(MAX_VAL)
	lmax4.PushBack(MAX_VAL)
	lmax4.PushBack(MAX_VAL)
	lmax4.PushBack(MAX_VAL)

	lmax5.PushBack(MAX_VAL)
	lmax5.PushBack(MAX_VAL)
	lmax5.PushBack(MAX_VAL)
	lmax5.PushBack(MAX_VAL)
	lmax5.PushBack(MAX_VAL)

	lmin1.PushBack(MIN_VAL)

	lmin2.PushBack(MIN_VAL)
	lmin2.PushBack(MIN_VAL)

	lmin3.PushBack(MIN_VAL)
	lmin3.PushBack(MIN_VAL)
	lmin3.PushBack(MIN_VAL)

	lmin4.PushBack(MIN_VAL)
	lmin4.PushBack(MIN_VAL)
	lmin4.PushBack(MIN_VAL)
	lmin4.PushBack(MIN_VAL)

	lmin5.PushBack(MIN_VAL)
	lmin5.PushBack(MIN_VAL)
	lmin5.PushBack(MIN_VAL)
	lmin5.PushBack(MIN_VAL)
	lmin5.PushBack(MIN_VAL)

	x := Constants{
		BYTES_OF_INFIMUM:  []byte("infimum"), //infimum\0
		BYTES_OF_SUPREMUM: []byte("supremum"),
		MAX_RECORD_1:      lmax1,
		MAX_RECORD_2:      lmax2,
		MAX_RECORD_3:      lmax3,
		MAX_RECORD_4:      lmax4,
		MAX_RECORD_5:      lmax5,
		MIN_RECORD_1:      lmin1,
		MIN_RECORD_2:      lmin2,
		MIN_RECORD_3:      lmin3,
		MIN_RECORD_4:      lmin4,
		MIN_RECORD_5:      lmin5,
		CONST_UNSIGNED:    unsigned,
	}
	return x
}
